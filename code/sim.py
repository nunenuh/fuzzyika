import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt

# Generate universe variables
# input Set dan Range
x_ch = np.arange(0, 141, 0.1)
x_da = np.arange(0, 21, 0.01)
x_tma = np.arange(0, 2.6, 0.01)
x_ka = np.arange(0, 1801, 0.1)
x_banjir = np.arange(0, 101, 0.1)


# Generate fuzzy membership functions
ch_th = fuzz.trimf(x_ch, [0, 0, 5])
ch_hr = fuzz.trimf(x_ch, [0, 5, 20])
ch_hs = fuzz.trimf(x_ch, [5, 20, 50])
ch_hl = fuzz.trimf(x_ch, [20, 50, 100])
ch_sl = fuzz.trimf(x_ch, [50, 100, 140])

da_lo = fuzz.trimf(x_da, [0, 0, 0.39])
da_md = fuzz.trimf(x_da, [0, 0.39, 9.40])
da_hi = fuzz.trimf(x_da, [0.39, 9.40, 20])

tma_lo = fuzz.trimf(x_tma, [0, 0, 0.66])
tma_md = fuzz.trimf(x_tma, [0, 0.66, 1.41])
tma_hi = fuzz.trimf(x_tma, [0.66, 1.41, 2.5])

ka_lo = fuzz.trimf(x_ka, [0, 0, 2])
ka_md = fuzz.trimf(x_ka, [0, 2, 768])
ka_hi = fuzz.trimf(x_ka, [2, 768, 1800])

banjir_lo = fuzz.trimf(x_banjir, [0, 0, 25])
banjir_md = fuzz.trimf(x_banjir, [0, 25, 50])
banjir_hi = fuzz.trimf(x_banjir, [25, 50, 100])

# Visualize these universes and membership functions
fig, (ax0, ax1, ax2, ax3, ax4) = plt.subplots(nrows=5, figsize=(8, 9))

ax0.plot(x_ch, ch_th, 'b', linewidth=1.5, label='Tidak Hujan')
ax0.plot(x_ch, ch_hr, 'g', linewidth=1.5, label='Hujan Ringan')
ax0.plot(x_ch, ch_hs, 'r', linewidth=1.5, label='Hujan Sedang')
ax0.plot(x_ch, ch_hl, 'r', linewidth=1.5, label='Hujan Lebat')
ax0.plot(x_ch, ch_sl, 'r', linewidth=1.5, label='Hujan Sangat Lebat')

ax0.set_title('Curah Hujan')
ax0.legend()


# Turn off top/right axes
for ax in (ax0, ax1, ax2):
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    plt.tight_layout()


