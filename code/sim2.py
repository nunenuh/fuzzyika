import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt
from skfuzzy import control as ctrl
import time


# Generate universe variables
# input Set dan Range

x_ch = np.arange(0, 141, 0.1)
ch = ctrl.Antecedent(np.arange(0, 141, 0.1), 'curah_hujan')

x_da = np.arange(0, 21, 0.01)
da = ctrl.Antecedent(np.arange(0, 21, 0.01), 'debit_air')

x_tma = np.arange(0, 2.6, 0.01)
tma = ctrl.Antecedent(np.arange(0, 2.6, 0.01), 'tinggi_muka_air')

x_ka = np.arange(0, 1801, 0.1)
ka = ctrl.Antecedent(np.arange(0, 1801, 0.1), 'kecepatan_angin')

x_banjir = np.arange(0, 101, 0.1)
banjir = ctrl.Consequent(np.arange(0, 101, 0.1), 'banjir')


# Generate fuzzy membership functions
ch['th'] = fuzz.trimf(ch.universe, [0, 0, 5])
ch['hr'] = fuzz.trimf(ch.universe, [0, 5, 20])
ch['hs'] = fuzz.trimf(ch.universe, [5, 20, 50])
ch['hl'] = fuzz.trimf(ch.universe, [20, 50, 100])
ch['sl'] = fuzz.trimf(ch.universe, [50, 100, 140])

da['low'] = fuzz.trimf(da.universe, [0, 0, 0.39])
da['medium'] = fuzz.trimf(da.universe, [0, 0.39, 9.40])
da['high'] = fuzz.trimf(da.universe, [0.39, 9.40, 20])

tma['low']= fuzz.trimf(tma.universe, [0, 0, 0.66])
tma['medium'] = fuzz.trimf(tma.universe, [0, 0.66, 1.41])
tma['high'] = fuzz.trimf(tma.universe, [0.66, 1.41, 2.5])

ka['low'] = fuzz.trimf(ka.universe, [0, 0, 2])
ka['medium'] = fuzz.trimf(ka.universe, [0, 2, 768])
ka['high'] = fuzz.trimf(ka.universe, [2, 768, 1800])

banjir['low'] = fuzz.trimf(banjir.universe, [0, 0, 25])
banjir['medium'] = fuzz.trimf(banjir.universe, [0, 25, 50])
banjir['high'] = fuzz.trimf(banjir.universe, [25, 50, 100])

ch['hs'].view()
da['medium'].view()
tma['medium'].view()
ka['medium'].view()
banjir['medium'].view()


# Pembuatan Rule dan simulasi system
rule1 = ctrl.Rule(ch['hl'], banjir['medium'])
rule2 = ctrl.Rule(ch['hr'] & tma['low'], banjir['low'])

banjir_ctrl = ctrl.ControlSystem([rule1, rule2, rule3])
simulasi = ctrl.ControlSystemSimulation(banjir_ctrl)

simulasi.input['curah_hujan'] = 80
simulasi.input['kecepatan_angin'] = 50
simulasi.input['tinggi_muka_air'] = 1.5
simulasi.input['debit_air'] = 0.5

# Crunch the numbers
simulasi.compute()
print simulasi.output['banjir']
banjir.view(sim=simulasi)

time.sleep(1000)